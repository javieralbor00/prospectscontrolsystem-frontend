import Vue from 'vue';
import Router from 'vue-router';
import ProspectList from '@/components/ProspectList';
import CreateProspect from '@/components/CreateProspect';
import DetailProspect from '@/components/DetailProspect';
import Historic from '@/components/Historic';
import Login from '@/views/Login';
import Register from '@/views/Register';
import store from '../store';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '#', redirect: '/home' },
    {
      path: '/home',
      name: 'prospect-list',
      component: ProspectList,
      meta: {
        auth: true,
      },
    },
    {
      path: '/create-prospect',
      name: 'createProspect',
      component: CreateProspect,
      meta: {
        auth: true,
      },
    },
    {
      path: '/historic',
      name: 'historic',
      component: Historic,
      props: true,
      meta: {
        auth: true,
      },
    },
    {
      path: '/detail-prospect',
      name: 'detailProspect',
      component: DetailProspect,
      meta: {
        auth: true,
      },
      props: true,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/register',
      name: 'Register',
      component: Register,
    },
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.length > 0) {
    next();
  } else {
    next('/home');
  }
  if (to.matched.some(record => record.meta.auth) && !store.state.token) {
    next('/login');
  } else {
    next();
  }
});
export default router;
